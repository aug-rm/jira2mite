### What is this repository for? ###

This Atlassian JIRA Plugin provides synchronization of JIRA worklogs into Mite.

Feature list will be maintained in
TBD

Task list is currently contained in this Bitbucket. See here
TBD

Here are the Atlassian SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:
https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK