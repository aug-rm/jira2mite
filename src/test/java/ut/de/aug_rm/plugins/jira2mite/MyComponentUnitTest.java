package ut.de.aug_rm.plugins.jira2mite;

import org.junit.Test;
import de.aug_rm.plugins.jira2mite.api.MyPluginComponent;
import de.aug_rm.plugins.jira2mite.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}